package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/api", func(c *gin.Context) {
		c.String(200, "Hello from Meetup 22-04!")
	})
	return r
}

func main() {
	r := setupRouter()

	err := r.Run(":80")

	if err != nil {
		fmt.Print("running router")
	} else {
		fmt.Printf("err %s", err)
	}
}
