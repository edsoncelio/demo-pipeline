FROM golang:1.18.1-alpine3.15

#checkov:skip=CKV_DOCKER_2
RUN apk add git=2.34.2-r0 --no-cache

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -v -o /usr/local/bin/app ./...

#checkov:skip=CKV_DOCKER_3
EXPOSE 8080

CMD ["app"]
